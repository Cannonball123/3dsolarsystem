package application;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;

import entities.Camera;
import entities.Planet;
import entities.Entity;
import entities.Light;
import entities.Player;
import entities.Star;
import models.RawModel;
import models.TexturedModel;
import renderEngine.DisplayManager;
import renderEngine.Loader;
import renderEngine.MasterRenderer;
import renderEngine.OBJLoader;
import renderEngine.PLNLoader;
import textures.ModelTexture;

public class MainGameLoop {
	
	public static void main(String[] args) {
		
		DisplayManager.createDisplay();
		Loader loader = new Loader();
		MasterRenderer masterRenderer = new MasterRenderer();
		
		// Entity models
		RawModel rawSun = OBJLoader.loadObjModel("sun", loader);
		ModelTexture sunTexture = new ModelTexture(loader.loadTexture("sun2k"));
		TexturedModel texturedSun = new TexturedModel(rawSun, sunTexture);
		sunTexture.setLightSource(true);
		sunTexture.setShineDamper(0.5f);
		sunTexture.setReflectivity(1);
		
		RawModel rawMercury = OBJLoader.loadObjModel("mercury", loader);
		ModelTexture mercuryTexture = new ModelTexture(loader.loadTexture("mercury2k"));
		TexturedModel texturedMercury = new TexturedModel(rawMercury, mercuryTexture);
		
		RawModel rawVenus = OBJLoader.loadObjModel("venus", loader);
		ModelTexture venusTexture = new ModelTexture(loader.loadTexture("venus2k"));
		TexturedModel texturedVenus = new TexturedModel(rawVenus, venusTexture);
		
		RawModel rawEarth = OBJLoader.loadObjModel("earth", loader);
		ModelTexture earthTexture = new ModelTexture(loader.loadTexture("earth2k"));
		TexturedModel texturedEarth = new TexturedModel(rawEarth, earthTexture);
//		earthTexture.setShineDamper(10);
//		earthTexture.setReflectivity(1);
		
		RawModel rawMars = OBJLoader.loadObjModel("mars", loader);
		ModelTexture marsTexture = new ModelTexture(loader.loadTexture("mars2k"));
		TexturedModel texturedMars = new TexturedModel(rawMars, marsTexture);
		
		RawModel rawJupiter = OBJLoader.loadObjModel("jupiter", loader);
		ModelTexture jupiterTexture = new ModelTexture(loader.loadTexture("jupiter2k"));
		TexturedModel texturedJupiter = new TexturedModel(rawJupiter, jupiterTexture);
		
		RawModel rawSaturn = OBJLoader.loadObjModel("saturn", loader);
		ModelTexture saturnTexture = new ModelTexture(loader.loadTexture("saturn2k"));
		TexturedModel texturedSaturn = new TexturedModel(rawSaturn, saturnTexture);
		
		RawModel rawSaturnRings = OBJLoader.loadObjModel("saturn_rings", loader);
		ModelTexture saturnRingsTexture = new ModelTexture(loader.loadTexture("saturn_rings2k"));
		TexturedModel texturedSaturnRings = new TexturedModel(rawSaturnRings, saturnRingsTexture);
		saturnRingsTexture.setTransparent(true);
		
		RawModel rawUranus = OBJLoader.loadObjModel("uranus", loader);
		ModelTexture uranusTexture = new ModelTexture(loader.loadTexture("uranus2k"));
		TexturedModel texturedUranus = new TexturedModel(rawUranus, uranusTexture);
		
		RawModel rawNeptune = OBJLoader.loadObjModel("neptune", loader);
		ModelTexture neptuneTexture = new ModelTexture(loader.loadTexture("neptune2k"));
		TexturedModel texturedNeptune = new TexturedModel(rawNeptune, neptuneTexture);
		
		// Plane models
		RawModel rawPlane = PLNLoader.generatePlane(loader);
		ModelTexture planeTexture = new ModelTexture(loader.loadTexture("grid"));
		TexturedModel texturedPlane = new TexturedModel(rawPlane, planeTexture);
		
		
		List<Entity> entities = new ArrayList<Entity>();
		//List<Plane> planes = new ArrayList<Plane>();
		
		// RELATIVE TO EARTH SCALE AND ECLIPTIC, CENTER IS THE SUN
		
		Star sun = new Star("Sun", texturedSun, texturedPlane, new Vector3f(0, 0, 0), new Vector3f(0,0,0), 30f,
				0.039293528f, 7.25f, 0f);
		Planet mercury = new Planet("Mercury", texturedMercury, null, new Vector3f(-160, 0, 0), new Vector3f(0,0,0), 0.3825f,
				0.01700484f, sun, 4.152091f, 0.0f, 7.00f);
		Planet venus = new Planet("Venus", texturedVenus, null, new Vector3f(-180, 0, 0), new Vector3f(0,0,0), 0.9488f,
				0.0041036746f, sun, 1.6255232f, 177.3f, 3.39f);
		Planet earth = new Planet("Earth", texturedEarth, null, new Vector3f(-200, 0, 0), new Vector3f(0,0,0), 1f,
				1.0f, sun, 1.0f, 23.44f, 0.0f);
		Planet mars = new Planet("Mars", texturedMars, null, new Vector3f(-220, 0, 0), new Vector3f(0,0,0), 0.53260f,
				0.97203875f, sun, 0.53168446f, 25.19f, 1.85f);
		Planet jupiter = new Planet("Jupiter", texturedJupiter, null, new Vector3f(-300, 0, 0), new Vector3f(0,0,0), 11.209f,
				2.4115434f, sun, 0.084299915f, 3.12f, 1.31f);
		Planet saturn = new Planet("Saturn", texturedSaturn, null, new Vector3f(-400, 0, 0), new Vector3f(0,0,0), 9.449f,
				2.2460523f, sun, 0.033959333f, 26.73f, 2.48f);
		Planet saturnRings = new Planet("Saturn Rings", texturedSaturnRings, null, new Vector3f(-400, 0, 0), new Vector3f(0,0,0), 9.449f,
				2.2460523f, sun, 0.033959333f, 26.73f, 2.48f); //to update once satellites are created
		Planet uranus = new Planet("Uranus", texturedUranus, null, new Vector3f(-500, 0, 0), new Vector3f(0,0,0), 4.007f,
				1.3883169f, sun, 0.011902582f, 97.86f, 0.76f);
		Planet neptune = new Planet("Neptune", texturedNeptune, null, new Vector3f(-600, 0, 0), new Vector3f(0,0,0), 3.883f,
				1.4856905f, sun, 0.006068386f, 28.32f, 1.77f);
		
		entities.add(sun);
		entities.add(mercury);
		entities.add(venus);
		entities.add(earth);
		entities.add(mars);
		entities.add(jupiter);
		entities.add(saturn);
		entities.add(saturnRings);
		entities.add(uranus);
		entities.add(neptune);
		
		RawModel rawPlayer = OBJLoader.loadObjModel("player", loader);
		ModelTexture playerTexture = new ModelTexture(loader.loadTexture("player"));
		TexturedModel texturedPlayer = new TexturedModel(rawPlayer, playerTexture);
		Player player = new Player("Player", texturedPlayer, null, new Vector3f(-200, 0, -500), new Vector3f(0, 0, 0), 1f, 0.0f, 0.0f);
		
		entities.add(player);
		
		Light sunLight = new Light(new Vector3f(0, 0, 0), new Vector3f(1,1,1));
		Camera camera = new Camera(player, new Vector3f(-200, 40, -600));
		
		while(!Display.isCloseRequested()) {
			camera.move();
			
			for(Entity entity:entities) {
				entity.move();
				masterRenderer.processEntity(entity);
			}
			
			masterRenderer.render(sunLight, camera);
			
			DisplayManager.updateDisplay();
			
		}
		
		masterRenderer.cleanUp();
		loader.cleanUP();
		DisplayManager.closeDisplay();
		
	}

}
