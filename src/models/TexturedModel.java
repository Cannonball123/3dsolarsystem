package models;

import textures.ModelTexture;

public class TexturedModel {
	
	/**
	 * A class that keeps information about a textured model,<br>
	 * namely, an object that has a model, and also has a texture.
	 */
	
	private RawModel rawModel;
	private ModelTexture modelTexture;
	
	public TexturedModel(RawModel rawModel, ModelTexture modelTexture) {
		this.rawModel = rawModel;
		this.modelTexture = modelTexture;
	}

	public RawModel getRawModel() {
		return rawModel;
	}

	public ModelTexture getModelTexture() {
		return modelTexture;
	}

}
