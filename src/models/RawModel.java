package models;

public class RawModel {
	
	/**
	 * A class that keeps information about a model VAO.
	 * It keeps the id of the VAO and its vertex count.
	 */
	
	private int vaoID;
	private int vertexCount;
	
	public RawModel(int vaoID, int vertexCount) {
		this.vaoID = vaoID;
		this.vertexCount = vertexCount;
	}

	public int getVaoID() {
		return vaoID;
	}

	public int getVertexCount() {
		return vertexCount;
	}
	
	
	
	

}
