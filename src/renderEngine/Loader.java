package renderEngine;

import java.io.FileInputStream;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import models.RawModel;

public class Loader {
	
	private List<Integer> vaos = new ArrayList<Integer>();
	private List<Integer> vbos = new ArrayList<Integer>();
	private List<Integer> textures = new ArrayList<Integer>();
	
	/**
	 * Takes in a model's positions, texture coordinates,<br>
	 * normals and vertex indices, loads them into a fresh VAO<br>
	 * and returns information about the VAO as a RawModel.
	 * 
	 * @param positions		- Vertex positions of a model 
	 * @param textureCoords - The model's texture coordinates
	 * @param normals		- The model's nomals coordinates
	 * @param indices 		- The model's vertex indices
	 * @return Information about the model's VAO as a RawModel
	 */
	public RawModel loadToVAO(float[] positions, float[] textureCoords, float[] normals, int[] indices) {
		int vaoID = createVAO();
		bindVAO(vaoID);
		bindIndicesBuffer(indices);
		storeDataInAttributeList(0, 3, positions);
		storeDataInAttributeList(1, 2, textureCoords);
		storeDataInAttributeList(2, 3, normals);
		unbindVAO();
		return new RawModel(vaoID, indices.length);
	}
	
	/**
	 * Loads to memory a texture from an image file and returns its<br>
	 * ID so we can refer to it.<br>
	 * Also stores the texture ID in a list that can be deleted at<br>
	 * the end of the game.
	 * 
	 * @param fileName - The name of the image file
	 * @return The ID of the loaded texture object 
	 */
	public int loadTexture(String fileName) {
		Texture texture = null;
		try {
			texture = TextureLoader.getTexture("PNG", new FileInputStream("res/"+fileName+".png"));
			GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);
			GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL14.GL_TEXTURE_LOD_BIAS, -1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		int textureID = texture.getTextureID();
		textures.add(textureID);
		return textureID;
	}
	
	/**
	 * Create an empty Vertex Arrays Object (VAO),<br>
	 * add its id to the VAO list (vaos) and return the id.
	 * 
	 * @return The VAO ID
	 */
	private int createVAO() {
		int vaoID = GL30.glGenVertexArrays();
		vaos.add(vaoID);
		return vaoID;
	}
	
	/**
	 * Activate a VAO with vaoID by binding it.
	 * 
	 * @param vaoID - The ID of the VAO to bind.
	 */
	private void bindVAO(int vaoID) {
		GL30.glBindVertexArray(vaoID);		
	}
	
	/**
	 * Deactivate the current active VAO by unbinding it.
	 */
	private void unbindVAO() {
		GL30.glBindVertexArray(0);
	}
	
	/**
	 * Store data represented as a float array into the<br>
	 * attributeNumber'th vertex buffer object (VBO) of the current<br>
	 * active VAO.<br>
	 * Also, since a new VBO is generated, add its id to the VBO<br>
	 * list (vbos).
	 * 
	 * @param attributeNumber - The VBO number in the VAO to store in.
	 * @param coordinateSize - The number of floats in the data array<br>
	 * 						   that represent one coordinate.
	 * @param data - The array of floats to store.
	 */
	private void storeDataInAttributeList(int attributeNumber, int coordinateSize, float[] data) {
		int vboID = GL15.glGenBuffers();
		vbos.add(vboID);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
		FloatBuffer buffer = storeDataInFloatBuffer(data);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
		GL20.glVertexAttribPointer(attributeNumber, coordinateSize, GL11.GL_FLOAT, false, 0, 0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
	}
	
	/**
	 * Stores an array of floats in a float buffer.
	 * @param data - The floats array to be stored
	 * @return A FloatBuffer object containing the floats<br>
	 * 		   from the array, ready to be read from
	 */
	private FloatBuffer storeDataInFloatBuffer(float[] data) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	/**
	 * Loads and binds an indices array to the active VAO.
	 * 
	 * @param indices - The indices array to be loaded
	 */
	private void bindIndicesBuffer(int[] indices) {
		int vboID = GL15.glGenBuffers();
		vbos.add(vboID);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboID);
		IntBuffer buffer = storeDataInIntBuffer(indices);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
	}
	
	/**
	 * Stores an array of ints in an int buffer.
	 * @param data - The ints array to be stored
	 * @return An IntBuffer object containing the ints<br>
	 * 		   from the array, ready to be read from
	 */
	private IntBuffer storeDataInIntBuffer(int[] data) {
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	/**
	 * Deletes the VAO's and the VBO's lists.
	 */
	public void cleanUP() {
		for(int vao:vaos) {
			GL30.glDeleteVertexArrays(vao);
		}
		for(int vbo:vbos) {
			GL15.glDeleteBuffers(vbo);
		}
		for(int texture:textures) {
			GL11.glDeleteTextures(texture);
		}
	}
	

}
