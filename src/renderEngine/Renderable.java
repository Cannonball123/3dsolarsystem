package renderEngine;

import org.lwjgl.util.vector.Vector3f;

public interface Renderable {
	
	public Vector3f getPosition();
	
	public Vector3f getRotation();
	
	public Vector3f getScale();
	
	public float getAxialTilt();

}
