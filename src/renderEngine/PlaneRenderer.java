package renderEngine;

import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;

import models.RawModel;
import planes.Plane;
import shaders.PlaneShader;
import textures.ModelTexture;
import toolbox.Maths;

public class PlaneRenderer {
	
	private PlaneShader planeShader;
	
	public PlaneRenderer(PlaneShader planeShader, Matrix4f projectionMatrix) {
		this.planeShader = planeShader;
		this.planeShader.start();
		this.planeShader.loadProjectionMatrix(projectionMatrix);
		this.planeShader.stop();
	}
	
	public void render(List<Plane> planes) {
		for(Plane plane:planes) {
			prepareTexturedModel(plane);
			loadMatrices(plane);
			GL11.glDrawElements(GL11.GL_TRIANGLES, plane.getTexturedModel().getRawModel().getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
			unbindTexturedModel();
		}
	}
	
	private void prepareTexturedModel(Plane plane) {
		RawModel rawModel = plane.getTexturedModel().getRawModel();
		GL30.glBindVertexArray(rawModel.getVaoID());
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);
		ModelTexture modelTexture = plane.getTexturedModel().getModelTexture();
		MasterRenderer.disableCulling();
		this.planeShader.loadLightSourceVariable(modelTexture.isLightSource());
		this.planeShader.loadSpecularLighting(modelTexture.getShineDamper(), modelTexture.getReflectivity());
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, modelTexture.getTextureID());
	}
	
	private void unbindTexturedModel() {
		MasterRenderer.enableCulling();
		GL20.glDisableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);
		GL20.glDisableVertexAttribArray(2);
		GL30.glBindVertexArray(0);
	}
	
	private void loadMatrices(Plane plane) {
		Matrix4f transformationMatrix = Maths.createTransformationMatrix(plane);
		this.planeShader.loadTransformationMatrix(transformationMatrix);
		this.planeShader.loadOrbitalInclinationMatrix(plane.getOrbitalInclinationMatrix());
		
//		entityShader.loadTransformationMatrix(entity.getTransformationMatrix());
	}

}
