package renderEngine;

import java.util.List;
import java.util.Map;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;

import entities.Entity;
import models.RawModel;
import models.TexturedModel;
import shaders.EntityShader;
import textures.ModelTexture;
import toolbox.Maths;

public class EntityRenderer {
	
	private EntityShader entityShader;
	
	/**
	 * Constructs the renderer and initializes the projection<br>
	 * matrix. It is done here because there is no need to<br>
	 * load the projection matrix again while the game runs.
	 * 
	 * @param entityShader - The shader into which to load<br>
	 * 						 the projection matrix.
	 */
	public EntityRenderer(EntityShader entityShader, Matrix4f projectionMatrix) {
		this.entityShader = entityShader;
		entityShader.start();
		entityShader.loadProjectionMatrix(projectionMatrix);
		entityShader.stop();
	}
	
	public void render(Map<TexturedModel, List<Entity>> entities) {
		for(TexturedModel texturedModel:entities.keySet()) {
			prepareTexturedModel(texturedModel);
			List<Entity> batch = entities.get(texturedModel);
			for(Entity entity:batch) {
				loadMatrices(entity);
				GL11.glDrawElements(GL11.GL_TRIANGLES, texturedModel.getRawModel().getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
			}
			unbindTexturedModel();
		}
	}
	
	private void prepareTexturedModel(TexturedModel texturedModel) {
		RawModel rawModel = texturedModel.getRawModel();
		GL30.glBindVertexArray(rawModel.getVaoID());
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);
		ModelTexture modelTexture = texturedModel.getModelTexture();
		if(modelTexture.isTransparent()) {
			MasterRenderer.disableCulling();
		}
		entityShader.loadLightSourceVariable(modelTexture.isLightSource());
		entityShader.loadSpecularLighting(modelTexture.getShineDamper(), modelTexture.getReflectivity());
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, modelTexture.getTextureID());
	}
	
	private void unbindTexturedModel() {
		MasterRenderer.enableCulling();
		GL20.glDisableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);
		GL20.glDisableVertexAttribArray(2);
		GL30.glBindVertexArray(0);
	}
	
	private void loadMatrices(Entity entity) {
//		Matrix4f transformationMatrix = Maths.createTransformationMatrix(entity);
//		entityShader.loadTransformationMatrix(transformationMatrix);
		entityShader.loadTransformationMatrix(entity.getTransformationMatrix());
		
		entityShader.loadOrbitalInclinationMatrix(entity.getOrbitalInclinationMatrix());
		
	}
	

}
