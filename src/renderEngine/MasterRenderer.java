package renderEngine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;

import entities.Camera;
import entities.Entity;
import entities.Light;
import models.TexturedModel;
import planes.Plane;
import shaders.EntityShader;
import shaders.PlaneShader;
import toolbox.Maths;

public class MasterRenderer {
	
	/**
	 * Handles all of the rendering in the game
	 */
	
	private static final float FIELD_OF_VIEW = 70;
	private static final float NEAR_PLANE = 0.1f;
	private static final float FAR_PLANE = 149597890f;
	
	private static final float RED = 0.01f;
	private static final float GREEN = 0.01f;
	private static final float BLUE = 0.01f;
	
	private Matrix4f projectionMatrix;
	
	private EntityShader entityShader = new EntityShader();
	private EntityRenderer entityRenderer;
	
	private PlaneShader planeShader = new PlaneShader();
	private PlaneRenderer planeRenderer;
	
	private Map<TexturedModel, List<Entity>> entities = new HashMap<TexturedModel, List<Entity>>();
	private List<Plane> planes = new ArrayList<Plane>();
	
	public MasterRenderer() {
		enableCulling();
		projectionMatrix = Maths.createProjectionMatrix(FIELD_OF_VIEW, NEAR_PLANE, FAR_PLANE);
		entityRenderer = new EntityRenderer(entityShader, projectionMatrix);
		planeRenderer = new PlaneRenderer(planeShader, projectionMatrix);
	}
	
	public static void enableCulling() {
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glCullFace(GL11.GL_BACK);
	}
	
	public static void disableCulling() {
		GL11.glDisable(GL11.GL_CULL_FACE);
	}
	
	public void render(Light light, Camera camera) {
		prepare();
		
		Matrix4f viewMatrix = Maths.createViewMatrix(camera);
		
		// Entity rendering
		entityShader.start();
		entityShader.loadDiffuseLighting(light);
		entityShader.loadViewMatrix(viewMatrix);
		entityRenderer.render(entities);
		entityShader.stop();
		
		// Plane rendering
		planeShader.start();
		planeShader.loadDiffuseLighting(light);
		planeShader.loadViewMatrix(viewMatrix);
		planeRenderer.render(planes);
		planeShader.stop();
		
		planes.clear();
		entities.clear();
	}
	
	/**
	 * Called once every frame, prepares openGL to render the game.
	 */
	public void prepare() {
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT|GL11.GL_DEPTH_BUFFER_BIT);
		GL11.glClearColor(RED, GREEN, BLUE, 1);
	}
	
	public void processEntity(Entity entity) {
		TexturedModel entityModel = entity.getTexturedModel();
		List<Entity> batch = entities.get(entityModel);
		if(batch != null) {
			batch.add(entity);
		} else {
			List<Entity> newBatch = new ArrayList<Entity>();
			newBatch.add(entity);
			entities.put(entityModel, newBatch);
		}
		
		if(entity.hasPlane()) {
			planes.add(entity.getPlane());
		}
	}
	
	public void cleanUp() {
		entityShader.cleanUp();
		planeShader.cleanUp();
	}

}
