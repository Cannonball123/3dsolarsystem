package shaders;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public abstract class ShaderProgram {
	
	private int programID;
	private int vertexShaderID;
	private int fragmentShaderID;
	
	private static FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);
	
	/**
	 * Constructs, links, and validates a shader program using<br>
	 * a vertex shader and a fragment shader source code files.
	 * 
	 * @param vertexFile - Vertex shader source code file name
	 * @param fragmentFile - Fragment shader source code file name
	 */
	public ShaderProgram(String vertexFile, String fragmentFile) {
		vertexShaderID = loadShader(vertexFile, GL20.GL_VERTEX_SHADER);
		fragmentShaderID = loadShader(fragmentFile, GL20.GL_FRAGMENT_SHADER);
		programID = GL20.glCreateProgram();
		GL20.glAttachShader(programID, vertexShaderID);
		GL20.glAttachShader(programID, fragmentShaderID);
		bindAttributes();
		GL20.glLinkProgram(programID);
		GL20.glValidateProgram(programID);
		getAllUniformLocations();
	}
	
	/**
	 * To make sure that all of the shader classes implement<br>
	 * getting all of the uniform locations.
	 */
	protected abstract void getAllUniformLocations();
	
	/**
	 * Gets the location of a uniform variable in the shader<br>
	 * code.
	 * 
	 * @param uniformName - The name of the uniform variable<br>
	 * 						as it appears in the shader code
	 * @return An int that represents the location of the<br>
	 * 		   uniform variable.
	 */
	protected int getUniformLocation(String uniformName) {
		return GL20.glGetUniformLocation(programID, uniformName);
	}
	
	/**
	 * Starts this shader program - will be called whenever<br>
	 * we want to start using this shader program.
	 */
	public void start() {
		GL20.glUseProgram(programID);
	}
	
	/**
	 * Stops this shader program - will be called after we<br>
	 * are done using this shader program.
	 */
	public void stop() {
		GL20.glUseProgram(0);
	}
	
	/**
	 * Cleans up memory - first stops the shader program,<br>
	 * then detaches the vertex and fragment shaders from<br>
	 * the program and deletes them, and finally deletes<br>
	 * the shader program itself.
	 */
	public void cleanUp() {
		stop();
		GL20.glDetachShader(programID, vertexShaderID);
		GL20.glDetachShader(programID, fragmentShaderID);
		GL20.glDeleteShader(vertexShaderID);
		GL20.glDeleteShader(fragmentShaderID);
		GL20.glDeleteProgram(programID);
	}
	
	/**
	 * Links up the inputs to the shader programs to one of the<br>
	 * VAO's attributes.
	 */
	protected abstract void bindAttributes();
	
	/**
	 * Takes the number of the attribute list in the VAO<br>
	 * that we want to bind, and the variable name in<br>
	 * the shader code that we want to bind that attribute to.
	 * 
	 * @param attribute - The number of the attribute list
	 * @param variableName - The variable name in the shader code
	 */
	protected void bindAttribute(int attribute, String variableName) {
		GL20.glBindAttribLocation(programID, attribute, variableName);
	}
	
	/**
	 * Loads up a float value 'value' to the uniform variable<br>
	 * that has the location 'location'.
	 * 
	 * @param location - The location of the uniform variable
	 * @param value - The value to load
	 */
	protected void loadFloat(int location, float value) {
		GL20.glUniform1f(location,  value);
	}
	
	/**
	 * Loads up a vector value 'vector' to the uniform variable<br>
	 * that has the location 'location'.
	 * 
	 * @param location - The location of the uniform variable
	 * @param vector - The vector to load
	 */
	protected void loadVector(int location, Vector3f vector) {
		GL20.glUniform3f(location, vector.x, vector.y, vector.z);
	}
	
	/**
	 * Loads up a boolean value 'value' to the uniform variable<br>
	 * that has the location 'location'.
	 * 
	 * @param location - The location of the uniform variable
	 * @param value - The value to load
	 */
	protected void loadBoolean(int location, boolean value) {
		float toLoad = 0;
		if(value) toLoad = 1;
		GL20.glUniform1f(location, toLoad);
	}
	
	/**
	 * Loads up a matrix value 'matrix' to the uniform variable<br>
	 * that has the location 'location'.
	 * 
	 * @param location - The location of the uniform variable
	 * @param matrix - The matrix to load
	 */
	protected void loadMatrix(int location, Matrix4f matrix) {
		matrix.store(matrixBuffer);
		matrixBuffer.flip();
		GL20.glUniformMatrix4(location, false, matrixBuffer);
	}
	
	/**
	 * Loads shader source code files and compiles them<br>
	 * into a shader program. Prints stacktrace and error<br>
	 * if loading/compilation failed.
	 * 
	 * @param file - The source code file name
	 * @param type - The type of shader (vertex/fragment)
	 * @return The ID of the compiled shader
	 */
	private static int loadShader(String file, int type) {
		StringBuilder shaderSource = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line;
			while((line = reader.readLine()) != null) {
				shaderSource.append(line).append("\n");
			}
			reader.close();
		} catch (IOException e) {
			System.err.println("Could not read file!");
			e.printStackTrace();
			System.exit(-1);
		}
		int shaderID = GL20.glCreateShader(type);
		GL20.glShaderSource(shaderID, shaderSource);
		GL20.glCompileShader(shaderID);
		if(GL20.glGetShaderi(shaderID,  GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
			System.out.println(GL20.glGetShaderInfoLog(shaderID, 500));
			System.err.println("Could not compile shader.");
			System.exit(-1);
		}
		return shaderID;
	}
	
}
