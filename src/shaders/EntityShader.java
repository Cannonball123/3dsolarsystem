package shaders;

import org.lwjgl.util.vector.Matrix4f;

import entities.Light;

public class EntityShader extends ShaderProgram {

	private static final String VERTEX_FILE = "src/shaders/entityVertex.txt";
	private static final String FRAGMENT_FILE = "src/shaders/entityFragment.txt";
	
	private int location_transformationMatrix;
	private int location_viewMatrix;
	private int location_projectionMatrix;
	private int location_lightPosition;
	private int location_lightColor;
	private int location_shineDamper;
	private int location_reflectivity;
	private int location_isLightSource;
	private int location_orbitalInclinationMatrix;
	
	public EntityShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "textureCoords");
		super.bindAttribute(2, "normal");
	}

	@Override
	protected void getAllUniformLocations() {
		location_transformationMatrix = super.getUniformLocation("transformationMatrix");
		location_viewMatrix = super.getUniformLocation("viewMatrix");
		location_projectionMatrix = super.getUniformLocation("projectionMatrix");
		location_lightPosition = super.getUniformLocation("lightPosition");
		location_lightColor = super.getUniformLocation("lightColor");
		location_shineDamper = super.getUniformLocation("shineDamper");
		location_reflectivity = super.getUniformLocation("reflectivity");
		location_isLightSource = super.getUniformLocation("isLightSource");
		location_orbitalInclinationMatrix = super.getUniformLocation("orbitalInclinationMatrix");
		
	}
	
	public void loadOrbitalInclinationMatrix(Matrix4f matrix) {
		super.loadMatrix(location_orbitalInclinationMatrix, matrix);
	}
	
	public void loadTransformationMatrix(Matrix4f matrix) {
		super.loadMatrix(location_transformationMatrix, matrix);
	}
	
	public void loadViewMatrix(Matrix4f matrix) {
		super.loadMatrix(location_viewMatrix, matrix);
	}
	
	public void loadProjectionMatrix(Matrix4f matrix) {
		super.loadMatrix(location_projectionMatrix, matrix);
	}
	
	public void loadDiffuseLighting(Light light) {
		super.loadVector(location_lightPosition, light.getPosition());
		super.loadVector(location_lightColor, light.getColor());
	}
	
	public void loadSpecularLighting(float shineDamper, float reflectivity) {
		super.loadFloat(location_shineDamper, shineDamper);
		super.loadFloat(location_reflectivity, reflectivity);
	}
	
	public void loadLightSourceVariable(boolean isLightSource) {
		super.loadBoolean(location_isLightSource, isLightSource);
	}
	

}
