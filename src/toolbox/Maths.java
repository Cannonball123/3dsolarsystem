package toolbox;

import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import entities.Camera;
import renderEngine.Renderable;

public class Maths {
	
	public static Matrix4f createTransformationMatrix(Renderable renderable) {
		Matrix4f matrix = new Matrix4f();
		matrix.setIdentity();
		Matrix4f.translate(renderable.getPosition(), matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(renderable.getAxialTilt()), new Vector3f(0,0,1), matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(renderable.getRotation().x), new Vector3f(1,0,0), matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(renderable.getRotation().y), new Vector3f(0,1,0), matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(renderable.getRotation().z), new Vector3f(0,0,1), matrix, matrix);
		Matrix4f.scale(renderable.getScale(), matrix, matrix);
		return matrix;
	}
	
	public static Matrix4f createProjectionMatrix(float fov, float nearPlane, float farPlane){
		Matrix4f matrix = new Matrix4f();
        float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight();
        float y_scale = (float) ((1f / Math.tan(Math.toRadians(fov / 2f))) * aspectRatio);
        float x_scale = y_scale / aspectRatio;
        float frustum_length = farPlane - nearPlane;
 
        matrix = new Matrix4f();
        matrix.m00 = x_scale;
        matrix.m11 = y_scale;
        matrix.m22 = -((farPlane + nearPlane) / frustum_length);
        matrix.m23 = -1;
        matrix.m32 = -((2 * nearPlane * farPlane) / frustum_length);
        matrix.m33 = 0;
        
        return matrix;
    }
	
	public static Matrix4f createViewMatrix(Camera camera) {
        Matrix4f viewMatrix = new Matrix4f();
        viewMatrix.setIdentity();
        Matrix4f.rotate((float) Math.toRadians(camera.getPitch()), new Vector3f(1, 0, 0), viewMatrix, viewMatrix);
        Matrix4f.rotate((float) Math.toRadians(camera.getYaw()), new Vector3f(0, 1, 0), viewMatrix, viewMatrix);
        Matrix4f.rotate((float) Math.toRadians(camera.getRoll()), new Vector3f(0, 0, 1), viewMatrix, viewMatrix);
        Vector3f cameraPos = camera.getPosition();
        Vector3f negativeCameraPos = new Vector3f(-cameraPos.x,-cameraPos.y,-cameraPos.z);
        Matrix4f.translate(negativeCameraPos, viewMatrix, viewMatrix);
        return viewMatrix;
    }

}
