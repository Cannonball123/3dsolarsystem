package planes;

import org.lwjgl.util.vector.Matrix4f;
//import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import models.TexturedModel;
import renderEngine.Renderable;
//import toolbox.Maths;

public class Plane implements Renderable{
	
	/**
	 * A class that represents a plane, which will be<br>
	 * every object rendered to the screen.
	 */
	private String name;
	
	private TexturedModel texturedModel;
	
	private Vector3f position;
	private Vector3f rotation;
	private Vector3f scale;
	
	protected float axialTilt; // in degrees
	protected Matrix4f orbitalInclinationMatrix;
	
//	private Matrix4f transformationMatrix;
	
	public Plane(String name, TexturedModel texturedModel, Vector3f position, Vector3f rotation, float scale, float axialTilt, float inclinationAngle) {
		this.name = name;
		
		this.texturedModel = texturedModel;
		
		this.position = position;
		this.rotation = rotation;
		this.scale = new Vector3f(scale, scale, scale);
		
		this.axialTilt = axialTilt;
		
		createOrbitalInclinationMatrix(inclinationAngle);
		
//		this.transformationMatrix = Maths.createTransformationMatrix(this.position, this.rotation, this.scale);
	}
	

	public String getName() {
		return name;
	}

	public TexturedModel getTexturedModel() {
		return texturedModel;
	}

	public void setTexturedModel(TexturedModel texturedModel) {
		this.texturedModel = texturedModel;
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
		
//System.out.println(this.position.toString() + "," + this.rotation.toString() + "," + this.scale.toString());
		
//		this.transformationMatrix = Maths.createTransformationMatrix(this.position, this.rotation, this.scale);
	}

	public Vector3f getRotation() {
		return rotation;
	}

	public void setRotation(Vector3f rotation) {
		this.rotation = rotation;
		
//		this.transformationMatrix = Maths.createTransformationMatrix(this.position, this.rotation, this.scale);
	}

	public Vector3f getScale() {
		return scale;
	}

	public void setScale(Vector3f scale) {
		this.scale = scale;
		
//		this.transformationMatrix = Maths.createTransformationMatrix(this.position, this.rotation, this.scale);
	}

	public float getAxialTilt() {
		return this.axialTilt;
	}

	public Matrix4f getOrbitalInclinationMatrix() {
		return orbitalInclinationMatrix;
	}
	
	public void changeRotation(float x, float y, float z) {
		this.rotation.x = (360 + (this.rotation.x + x)) % 360;
		this.rotation.y = (360 + (this.rotation.y + y)) % 360;
		this.rotation.z = (360 + (this.rotation.z + z)) % 360;
		
//		Matrix4f.rotate((float) Math.toRadians(x), new Vector3f(1,0,0), this.transformationMatrix, this.transformationMatrix);
//		Matrix4f.rotate((float) Math.toRadians(y), new Vector3f(0,1,0), this.transformationMatrix, this.transformationMatrix);
//		Matrix4f.rotate((float) Math.toRadians(z), new Vector3f(0,0,1), this.transformationMatrix, this.transformationMatrix);
		
	}
	
	
	private void createOrbitalInclinationMatrix(float inclinationAngle) {
		this.orbitalInclinationMatrix = new Matrix4f();
		this.orbitalInclinationMatrix.setIdentity();
		this.orbitalInclinationMatrix.m00 = (float) Math.cos(Math.toRadians(inclinationAngle));
		this.orbitalInclinationMatrix.m01 = (float) -Math.sin(Math.toRadians(inclinationAngle));
		this.orbitalInclinationMatrix.m10 = (float) Math.sin(Math.toRadians(inclinationAngle));
		this.orbitalInclinationMatrix.m11 = (float) Math.cos(Math.toRadians(inclinationAngle));
	}
	
	

}
