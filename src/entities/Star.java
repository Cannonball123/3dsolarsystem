package entities;

import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.util.Log;

import models.TexturedModel;
import planes.Plane;

public class Star extends Entity {

	private String name;
	
	private float rotationSpeed;
	
	public Star(String name, TexturedModel texturedModel, TexturedModel planeTexturedModel, Vector3f position, Vector3f rotation, float scale,
			 float rotationSpeed, float axialTilt, float inclinationAngle) {
		super(name, texturedModel, position, rotation, scale, axialTilt, inclinationAngle);
		
		if(planeTexturedModel != null) {
			this.plane = new Plane(name, planeTexturedModel, position, new Vector3f(0,0,0), 1, axialTilt, inclinationAngle);
		}
		
		this.name = name;
		this.rotationSpeed = rotationSpeed;
		
		Log.info(this.name + " created");
	}

	@Override
	public void move() {
		increaseRotation();
	}
	
	private void increaseRotation() {
		super.changeRotation(0, rotationSpeed, 0);
	}

}
