package entities;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

import models.TexturedModel;
import planes.Plane;
import renderEngine.DisplayManager;

public class Player extends Entity{
	
	private static enum moveMode {HOVER, FLIGHT};
	private static final float FLIGHT_SPEED = 40f;
	private static final float TURN_SPEED = 160f;
	
	private moveMode mode = moveMode.FLIGHT;
	private float currentFlightSpeed;
	private float currentYawSpeed;
	private float currentPitchSpeed;
	private float currentRollSpeed;

	public Player(String name, TexturedModel texturedModel, TexturedModel planeTexturedModel, Vector3f position, Vector3f rotation, float scale, float axialTilt, float inclinationAngle) {
		super(name, texturedModel, position, rotation, scale, axialTilt, inclinationAngle);
		
		if(planeTexturedModel != null) {
			this.plane = new Plane(name, planeTexturedModel, position, new Vector3f(0,0,0), 1, 0, inclinationAngle);
		}
		
	}
	
	public void move() {
		checkInputs();
		super.changeRotation(0, currentYawSpeed * DisplayManager.getFrameTimeSeconds(), 0);
//		super.plane.changeRotation(0, currentYawSpeed * DisplayManager.getFrameTimeSeconds(), 0);
		super.changeRotation(currentPitchSpeed * DisplayManager.getFrameTimeSeconds(), 0, 0);
//		super.plane.changeRotation(currentPitchSpeed * DisplayManager.getFrameTimeSeconds(), 0, 0);
		super.changeRotation(0, 0, currentRollSpeed * DisplayManager.getFrameTimeSeconds());
//		super.plane.changeRotation(0, 0, currentRollSpeed * DisplayManager.getFrameTimeSeconds());
		
//		System.out.println("position: " + this.position.toString() + ",   Rotation: " + this.rotation.toString());
		
		float distance = currentFlightSpeed * DisplayManager.getFrameTimeSeconds();
		
		float dx = (float) (distance * Math.sin(Math.toRadians(super.rotation.y)));
		
		float dy = (float) (distance * -Math.sin(Math.toRadians(super.rotation.x)));
		float dz = (float) (distance * Math.cos(Math.toRadians(super.rotation.y)));
		dy = 0;
//		System.out.println(distance + "," + super.getRotation().toString() + "," + dx + "," + dz);
		super.changePosition(dx, dy, dz);
		
//		super.changePosition(0, 0, distance);
	}
	
	public void checkInputs() {
		
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
			Vector3f rot = new Vector3f(0,0,0);
			Vector3f pos = new Vector3f(-200,0,-500);
			super.setPosition(pos);
			super.setRotation(rot);
			//super.plane.setPosition(pos);
			//super.plane.setRotation(rot);
			
		}
		
		while(Keyboard.next())
			if(Keyboard.getEventKeyState())
				if(Keyboard.getEventKey() == Keyboard.KEY_TAB)
					mode = (mode == moveMode.HOVER) ? moveMode.FLIGHT : moveMode.HOVER;
		
//		if(mode == moveMode.HOVER) {
//			
//			if(Keyboard.isKeyDown(Keyboard.KEY_W)) {
//				this.currentFlightSpeed = FLIGHT_SPEED;
//			} else if(Keyboard.isKeyDown(Keyboard.KEY_S)) {
//				this.currentFlightSpeed = -FLIGHT_SPEED;
//			} else {
//				this.currentFlightSpeed = 0;
//			}
//			
//			if(Keyboard.isKeyDown(Keyboard.KEY_A)) {
//				this.currentYawSpeed = TURN_SPEED;
//			} else if(Keyboard.isKeyDown(Keyboard.KEY_D)) {
//				this.currentYawSpeed = -TURN_SPEED;
//			} else {
//				this.currentYawSpeed = 0;
//			}
//			
////			if(Keyboard.isKeyDown(Keyboard.KEY_UP)) {
////				position.y += FLIGHT_SPEED;
////			}
////			if(Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
////				position.y -= FLIGHT_SPEED;
////			}
////			if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
////				position.x -= FLIGHT_SPEED;
////			}
////			if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
////				position.x += FLIGHT_SPEED;
////			}
//		}
		
		if(mode == moveMode.FLIGHT) {
			
			if(Keyboard.isKeyDown(Keyboard.KEY_W)) {
				this.currentFlightSpeed = FLIGHT_SPEED;
			} else if(Keyboard.isKeyDown(Keyboard.KEY_S)) {
				this.currentFlightSpeed = -FLIGHT_SPEED;
			} else {
				this.currentFlightSpeed = 0;
			}
			
			if(Keyboard.isKeyDown(Keyboard.KEY_A)) {
				this.currentYawSpeed = TURN_SPEED;
			} else if(Keyboard.isKeyDown(Keyboard.KEY_D)) {
				this.currentYawSpeed = -TURN_SPEED;
			} else {
				this.currentYawSpeed = 0;
			}
			
			if(Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
				this.currentPitchSpeed = -TURN_SPEED;
			} else if(Keyboard.isKeyDown(Keyboard.KEY_UP)) {
				this.currentPitchSpeed = TURN_SPEED;
			} else {
				this.currentPitchSpeed = 0;
			}
			
			if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
				this.currentRollSpeed = -TURN_SPEED;
			} else if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
				this.currentRollSpeed = TURN_SPEED;
			} else {
				this.currentRollSpeed = 0;
			}
		}
		
	}

}
