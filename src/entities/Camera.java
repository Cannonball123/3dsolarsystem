package entities;

import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public class Camera {
	
	private float distanceFromPlayer = 0f;
	private float angleAroundPlayer = 0f;
	private float angleAbovePlayer = 0f;
	
	private Vector3f position = new Vector3f(0,0,0);
	private float pitch = 20f;
	private float yaw = 180f;
	private float roll = 0f;
	
	private Player player;
	private float prx;
	private float pry;
	private float prz;
	
	public Camera(Player player) {
		this.player = player;
	}
	
	public Camera(Player player, Vector3f position) {
		this.player = player;
		this.position = position;
	}
	
	public void move() {

		System.out.print("Position:\n" + this.player.transformationMatrix.toString());
		System.out.println("Position m30:" + this.player.transformationMatrix.m30);
		System.out.println("Position m31:" + this.player.transformationMatrix.m31);
		System.out.println("Position m32:" + this.player.transformationMatrix.m32 + "\n");
		
		Matrix4f m = this.player.transformationMatrix;
		
		
		// code below seems to work for keeping camera rotated as player
		double rx;
		double ry;
		double rz;
		
		if(m.m02 != 1 && m.m02 != -1) {
			ry = - Math.asin(m.m02);
			rx = Math.atan2(m.m12/Math.cos(ry), m.m22/Math.cos(ry));
			rz = Math.atan2(m.m01/Math.cos(ry), m.m00/Math.cos(ry));
		} else {
			rz = 0;
			if(m.m02 == -1) {
				ry = Math.PI / 2;
				rx = rz + Math.atan2(m.m10, m.m20);
			} else {
				ry = -(Math.PI / 2);
				rx = -rz + Math.atan2(-m.m10, -m.m20);
			}
		}
		
		System.out.println("Rotation x:" + rx);
		System.out.println("Rotation y:" + ry);
		System.out.println("Rotation z:" + rz + "\n");
		
		this.prx = (float) Math.toDegrees(rx);
		this.pry = (float) Math.toDegrees(ry);
		this.prz = (float) Math.toDegrees(rz);
		
		this.pitch = (float) Math.toDegrees(rx);
		this.yaw = 180 -(float) Math.toDegrees(ry);
		this.roll = -(float) Math.toDegrees(rz);
		
		
		
//		calculateZoom();
//		calculateAngleAbovePlayer();
//		calculateAngleAroundPlayer();
//		float horizontalDistance = calculateHorizontalDistance();
//		float verticalDistance = calculateVerticalDistance();
//		calculateCameraPosition(horizontalDistance, verticalDistance);
//		this.yaw = 180 - (player.getRotation().y + angleAroundPlayer);
//		this.yaw = 180 -(float) Math.toDegrees(ry) - angleAroundPlayer;
		
		
		// The below code seems to work with the zoom
		this.position.x = m.m30;
		this.position.y = m.m31;
		this.position.z = m.m32;
		
		Matrix4f m2 = new Matrix4f();
		
		calculateAngleAbovePlayer();
		Matrix4f.rotate((float) Math.toRadians(angleAbovePlayer), new Vector3f(1,0,0), m, m2);
		
		// TODO: make it work
//		calculateAngleAroundPlayer();
//		Matrix4f.rotate((float) Math.toRadians(angleAroundPlayer), new Vector3f(0,1,0), m2, m2);
		
		calculateZoom();
		Matrix4f.translate(new Vector3f(0, 6, -20 - distanceFromPlayer), m2, m2);
		
		
		
		this.position.x += m2.m30;
		this.position.y += m2.m31;
		this.position.z += m2.m32;
		
		this.pitch += angleAbovePlayer;
//		this.yaw -= angleAroundPlayer;
		
		
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		this.pitch = pitch;
	}

	public float getYaw() {
		return yaw;
	}

	public void setYaw(float yaw) {
		this.yaw = yaw;
	}

	public float getRoll() {
		return roll;
	}

	public void setRoll(float roll) {
		this.roll = roll;
	}
	
	private void calculateCameraPosition(float horizontalDistance, float verticalDistance) {
		
//		float theta = player.getRotation().y + angleAroundPlayer;
//		float offsetX = (float) (horizontalDistance * Math.sin(Math.toRadians(theta)));
//		float offsetZ = (float) (horizontalDistance * Math.cos(Math.toRadians(theta)));
//		position.x = player.getPosition().x - offsetX;
//		position.z = player.getPosition().z - offsetZ;
//		position.y = player.getPosition().y + verticalDistance;
		
		
		float theta = pry + angleAroundPlayer;
		float offsetArX = (float) (horizontalDistance * Math.sin(Math.toRadians(theta)));
		float offsetArZ = (float) (horizontalDistance * Math.cos(Math.toRadians(theta)));
//		float psigh = prx + angleAbovePlayer;
//		float offsetAbY = (float) (verticalDistance * Math.sin(Math.toRadians(psigh)));
//		float offsetAbZ = (float) (horizontalDistance * Math.cos(Math.toRadians(psigh)));
		position.x = this.position.x - offsetArX;
		position.z = this.position.z - offsetArZ;
		position.y = this.position.y + verticalDistance;
	}
	
	private float calculateHorizontalDistance() {
		return (float) (distanceFromPlayer * Math.cos(Math.toRadians(angleAbovePlayer)));
	}
	
	private float calculateVerticalDistance() {
		return (float) (distanceFromPlayer * Math.sin(Math.toRadians(angleAbovePlayer)));
	}
	
	private void calculateZoom() {
		float zoomLevel = Mouse.getDWheel() * 0.1f;
		distanceFromPlayer -= zoomLevel;
		if(distanceFromPlayer < 25)
			distanceFromPlayer = 25;
		if(distanceFromPlayer > 1000)
			distanceFromPlayer = 1000;
	}
	
	private void calculateAngleAbovePlayer() {
		if(Mouse.isButtonDown(1)) {
			float angleChange = Mouse.getDY() * 0.1f;
			angleAbovePlayer -= angleChange;
			if(angleAbovePlayer < -90)
				angleAbovePlayer = -90;
			if(angleAbovePlayer > 90)
				angleAbovePlayer = 90;
		}
	}
	
	private void calculateAngleAroundPlayer() {
		if(Mouse.isButtonDown(0)) {
			float angleChange = Mouse.getDX() * 0.3f;
			angleAroundPlayer -= angleChange;
		}
	}
	

}
