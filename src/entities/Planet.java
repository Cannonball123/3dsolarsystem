package entities;

import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.util.Log;

import models.TexturedModel;
import planes.Plane;
import renderEngine.DisplayManager;

public class Planet extends Entity {
	
	private float rotationSpeed;
	private float selfRotation;
	
	private Star hostStar;
	private Vector3f baryCenter; // Position of the object that this planet orbits around.
	private float distanceFromBaryCenter;
	
	private Vector3f startPosition;
	private float orbitSpeed;
	private float orbitAngle;
	

	public Planet(String name, TexturedModel texturedModel, TexturedModel planeTexturedModel, Vector3f position, Vector3f rotation, float scale,
			 float rotationSpeed, Star hostStar, float orbitSpeed, float axialTilt, float inclinationAngle) {
		super(name, texturedModel, position, rotation, scale, axialTilt, inclinationAngle);
		
		if(planeTexturedModel != null) {
			this.plane = new Plane(name, planeTexturedModel, position, new Vector3f(0,0,0), 1, 0, inclinationAngle);
		}
		
		this.rotationSpeed = rotationSpeed;
		this.selfRotation = 0;
		
		this.hostStar = hostStar;
		this.baryCenter = this.hostStar.getPosition();
		this.distanceFromBaryCenter = Vector3f.sub(this.position, this.baryCenter, null).length();
		
		this.startPosition = super.position;
		this.orbitSpeed = orbitSpeed;
		this.orbitAngle = 0;
		
		
		Log.info(super.getName() + " created, " + this.getPosition() + ", " + this.baryCenter + ", " + this.distanceFromBaryCenter);
	}
	
	public void move() {
		increaseRotation();
		increaseOrbit();
	}
	
	private void increaseRotation() {
		this.selfRotation += rotationSpeed;
		super.changeRotation(0, this.selfRotation, 0);
	}
	
	private void increaseOrbit() {
		this.orbitAngle -= orbitSpeed * DisplayManager.getFrameTimeSeconds() * 10f;
		
		float newX = (float) (baryCenter.x + (startPosition.x - baryCenter.x)*Math.cos(Math.toRadians(orbitAngle)) - (startPosition.z - baryCenter.z)*Math.sin(Math.toRadians(orbitAngle)));
		float newZ = (float) (baryCenter.z + (startPosition.x - baryCenter.x)*Math.sin(Math.toRadians(orbitAngle)) + (startPosition.z - baryCenter.z)*Math.cos(Math.toRadians(orbitAngle)));
		
		super.setPosition(new Vector3f(newX, super.getPosition().y, newZ));
	}

}
