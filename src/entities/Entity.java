package entities;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import models.TexturedModel;
import planes.Plane;
import renderEngine.Renderable;
import toolbox.Maths;

public abstract class Entity implements Renderable {
	
	/**
	 * A class that represents an entity, which will be<br>
	 * every object rendered to the screen.
	 */
	protected String name;
	
	protected TexturedModel texturedModel;
	
	protected Plane plane;
	
	protected Vector3f position;
	protected Vector3f rotation;
	protected Vector3f scale;
	
	protected float axialTilt; // in degrees
	protected Matrix4f orbitalInclinationMatrix;
	
	public Matrix4f transformationMatrix;
	
	public Entity(String name, TexturedModel texturedModel, Vector3f position, Vector3f rotation, float scale, float axialTilt, float inclinationAngle) {
		this.name = name;
		
		this.texturedModel = texturedModel;
		
		this.plane = null;
		
		this.position = position;
		this.rotation = rotation;
		this.scale = new Vector3f(scale,scale,scale);
		
		this.axialTilt = axialTilt;
		
		createOrbitalInclinationMatrix(inclinationAngle);
		
		this.transformationMatrix = Maths.createTransformationMatrix(this);
	}
	
	
	public String getName() {
		return this.name;
	}
	
	public TexturedModel getTexturedModel() {
		return texturedModel;
	}
	
	public void setTexturedModel(TexturedModel texturedModel) {
		this.texturedModel = texturedModel;
	}
	
	public boolean hasPlane() {
		return this.plane != null;
	}
	
	public Plane getPlane() {
		return this.plane;
	}
	
	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
		
		//System.out.println(this.position.toString() + "," + this.rotation.toString() + "," + this.scale.toString());
		
		this.transformationMatrix = Maths.createTransformationMatrix(this);
	}
	
	public Vector3f getRotation() {
		return rotation;
	}

	public void setRotation(Vector3f rotation) {
		this.rotation = rotation;
		
		this.transformationMatrix = Maths.createTransformationMatrix(this);
	}
	
	public Vector3f getScale() {
		return scale;
	}

	public void setScale(Vector3f scale) {
		this.scale = scale;
		
		this.transformationMatrix = Maths.createTransformationMatrix(this);
	}
	
	public float getAxialTilt() {
		return this.axialTilt;
	}
	
	public Matrix4f getOrbitalInclinationMatrix() {
		return this.orbitalInclinationMatrix;
	}
	
	public abstract void move();
	
	public void changePosition(float x, float y, float z) {
		this.position.x = x;
		this.position.y = y;
		this.position.z = z;
		
		Matrix4f.translate(new Vector3f(x,y,z), this.transformationMatrix, this.transformationMatrix);
	}
	
	public void changeRotation(float x, float y, float z) {
//		this.rotation.x = (360 + (this.rotation.x + x)) % 360;
//		this.rotation.y = (360 + (this.rotation.y + y)) % 360;
//		this.rotation.z = (360 + (this.rotation.z + z)) % 360;
		
		this.rotation.x = x;
		this.rotation.y = y;
		this.rotation.z = z;
		
		Matrix4f.rotate((float) Math.toRadians(x), new Vector3f(1,0,0), this.transformationMatrix, this.transformationMatrix);
		Matrix4f.rotate((float) Math.toRadians(y), new Vector3f(0,1,0), this.transformationMatrix, this.transformationMatrix);
		Matrix4f.rotate((float) Math.toRadians(z), new Vector3f(0,0,1), this.transformationMatrix, this.transformationMatrix);
		
	}
	
	public void changeScale(float x, float y, float z) {
		this.scale.x = x;
		this.scale.y = y;
		this.scale.z = z;
		
		Matrix4f.scale(scale, this.transformationMatrix, this.transformationMatrix);
	}
	
	public void changeScale(float newScale) {
		this.scale.x = newScale;
		this.scale.y = newScale;
		this.scale.z = newScale;
		
		Matrix4f.scale(scale, this.transformationMatrix, this.transformationMatrix);
	}
	
	private void createOrbitalInclinationMatrix(float inclinationAngle) {
		this.orbitalInclinationMatrix = new Matrix4f();
		this.orbitalInclinationMatrix.setIdentity();
		this.orbitalInclinationMatrix.m00 = (float) Math.cos(Math.toRadians(inclinationAngle));
		this.orbitalInclinationMatrix.m01 = (float) -Math.sin(Math.toRadians(inclinationAngle));
		this.orbitalInclinationMatrix.m10 = (float) Math.sin(Math.toRadians(inclinationAngle));
		this.orbitalInclinationMatrix.m11 = (float) Math.cos(Math.toRadians(inclinationAngle));
	}

	public Matrix4f getTransformationMatrix() {
		return transformationMatrix;
	}

	public void setTransformationMatrix(Matrix4f transformationMatrix) {
		this.transformationMatrix = transformationMatrix;
	}
	
	
	

}
